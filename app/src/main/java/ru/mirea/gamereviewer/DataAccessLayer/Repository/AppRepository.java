package ru.mirea.gamereviewer.DataAccessLayer.Repository;

import android.app.Application;

import ru.mirea.gamereviewer.DataAccessLayer.Database.AppRoomDatabase;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.UserRepository;

public class AppRepository {
    private Application application;
    private AppRoomDatabase database;
    private UserRepository userRepository;

    AppRepository(Application application){
        this.application = application;

        database = AppRoomDatabase.getDatabase(application);
    }

    public UserRepository shareRepository(){
        if(this.userRepository != null)
            return this.userRepository;
        else
            return null;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void initializeUserRepository() {
        this.userRepository = new UserRepository(application);
    }
}
