package ru.mirea.gamereviewer.DataAccessLayer.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.GameDao;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Library.Library;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.ReviewDao;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.UserDao;


@Database(entities = {Game.class, User.class, Review.class}, version = 19)
public abstract class AppRoomDatabase extends RoomDatabase {
    public abstract GameDao gameDao();
    public abstract UserDao userDao();
    public abstract ReviewDao reviewDao();
    public abstract Library library();

    private static volatile AppRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static AppRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppRoomDatabase.class, "game_database")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}

//    private static AppDatabase instance;
//
//
//    private static final RoomDatabase.Callback roomcallback = new RoomDatabase.Callback(){
//        @Override
//        public void onCreate(@NonNull SupportSQLiteDatabase db) {
//            super.onCreate(db);
//            new PopulateDBAsyncTask(instance).execute();
//        }
//    };
//
//    private static class PopulateDBAsyncTask extends AsyncTask<Void, Void, Void>{
//        private GameDao gameDao;
//        public List<GameObj> gameObjs;
//
//        private PopulateDBAsyncTask(AppDatabase db){
//            gameDao = db.gameDao();
//        }
//
//        @Override
//        protected Void doInBackground(Void... voids) {
//            gameDao.insert(new Game(1, "1"));
//            gameDao.insert(new Game(2, "2"));
//            gameDao.insert(new Game(3, "3"));
//            gameDao.insert(new Game(4, "4"));
//            gameDao.insert(new Game(5, "5"));
//           //gameObjs = ApiManager.connectionWithUrl("https://api.rawg.io/api/games?key=4a0700d0745f466fbbacedd7e26da9fe");
//           //for (GameObj item:gameObjs) {
//           //    gameDao.insert(new Game(item.getId(), item.getName()));
//
//           //}
//            return null;
//        }
//    }
//}
