package ru.mirea.gamereviewer.DataAccessLayer.Room.Review;

import android.app.Application;

import ru.mirea.gamereviewer.DataAccessLayer.Database.AppRoomDatabase;

public class ReviewRepository {
    private AppRoomDatabase db;
    private ReviewDao reviewDao;

    public ReviewRepository(Application application){
        db = AppRoomDatabase.getDatabase(application);
        reviewDao = db.reviewDao();
    }

    public void addReview(Review review) {
        AppRoomDatabase.databaseWriteExecutor.execute( () -> {
            reviewDao.insert(review);
        });
    }
}
