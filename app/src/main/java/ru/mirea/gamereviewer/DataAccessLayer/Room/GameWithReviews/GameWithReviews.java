package ru.mirea.gamereviewer.DataAccessLayer.Room.GameWithReviews;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;

public class GameWithReviews {
    @Embedded
    public Game game;

    @Relation(
            parentColumn = "id",
            entityColumn = "gameID"
    )
    public List<Review> reviews;
}
