package ru.mirea.gamereviewer.DataAccessLayer.Room.Review;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;

@Entity(tableName = "reviews",
        foreignKeys = {
        @ForeignKey(
                entity = User.class,
                parentColumns = "id",
                childColumns = "userID",
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE
        ), @ForeignKey(
                entity = Game.class,
                parentColumns = "id",
                childColumns = "gameID",
                onUpdate = ForeignKey.CASCADE,
                onDelete = ForeignKey.CASCADE
        )
})
public class Review {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public String description;
    public float rating;

    public String gameName;
    public String gameImage;

    @ColumnInfo(index = true)
    public int userID;

    @ColumnInfo(index = true)
    public int gameID;


    public Review(String description, float rating, String gameName, String gameImage, int userID, int gameID){
        this.description = description;
        this.rating = rating;
        this.gameName = gameName;
        this.gameImage = gameImage;
        this.userID = userID;
        this.gameID = gameID;
    }


    /*public Review(String description, float rating, String game, int userID){
        this.description = description;
        this.rating = rating;
        this.game = game;
        this.userID = userID;
    }*/

    public Review(){

    }


    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }



    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }


    public float getRating() {
        return rating;
    }
    public void setRating(float rating) {
        this.rating = rating;
    }

}
