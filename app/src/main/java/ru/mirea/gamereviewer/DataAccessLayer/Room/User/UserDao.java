package ru.mirea.gamereviewer.DataAccessLayer.Room.User;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM users WHERE userName = :userName")
    LiveData<User> getUser(String userName);

    @Query("SELECT * FROM users WHERE id = :id")
    LiveData<List<User>> getUser(int id);

    /*@Query("SELECT * FROM users WHERE active = 1")
    User getActiveUser();*/

    @Query("SELECT * FROM users")
    LiveData<List<User>> getAllUsers();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(User user);

    @Delete
    void delete(User user);

    @Query("UPDATE users SET discord = :value WHERE id = :userID")
    void updateDiscord(String value, int userID);
    /*@Query("UPDATE users SET active = :value")
    void updateActive(boolean value);*/
}
