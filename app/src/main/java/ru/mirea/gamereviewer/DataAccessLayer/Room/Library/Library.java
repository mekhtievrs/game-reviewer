package ru.mirea.gamereviewer.DataAccessLayer.Room.Library;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.GameWithReviews.GameWithReviews;
import ru.mirea.gamereviewer.DataAccessLayer.Room.ReviewAndGame.ReviewAndGame;
import ru.mirea.gamereviewer.DataAccessLayer.Room.UserWithReviews.UserWithReviews;

@Dao
public interface Library {

    @Transaction
    @Query("SELECT * FROM users WHERE userName = :userName")
    LiveData<List<UserWithReviews>> getUsersReviews(String userName);

    @Transaction
    @Query("SELECT * FROM game_table WHERE name = :gameName")
    LiveData<List<GameWithReviews>> getGamesReviews(String gameName);

    /*@Transaction
    @Query("SELECT * FROM game_table")
    public LiveData<List<ReviewAndGame>> getReviewAndGame();*/

    /*@Transaction
    @Query("SELECT * FROM reviews")
    public LiveData<List<ReviewAndGame>> getReviewsAndGames();*/
}
