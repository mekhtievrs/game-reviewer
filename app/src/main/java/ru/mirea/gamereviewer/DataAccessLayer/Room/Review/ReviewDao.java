package ru.mirea.gamereviewer.DataAccessLayer.Room.Review;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;

@Dao
public interface ReviewDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Review review);

    @Delete
    void delete(Review review);
}
