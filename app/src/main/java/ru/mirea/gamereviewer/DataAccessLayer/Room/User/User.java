package ru.mirea.gamereviewer.DataAccessLayer.Room.User;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class User {
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String userName;
    public String nickName;
    public String steam;
    public String discord;

    public User(String userName, String nickName){
        this.userName = userName;
        this.nickName = nickName;
        this.steam = "";
        this.discord = "";
    }

    public User(){

    }
}
