package ru.mirea.gamereviewer.DataAccessLayer.Room.Game;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "game_table")
public class Game {
    @PrimaryKey(autoGenerate = true)
    public int id;
    // private String slug;
    public int uid;
    public String name;
    public String backgroundImage;
    public String released;
    public float rating;
    public int metacritic;
    public int playtime;
    public String publisher;
    public String developer;
    public String description;

    public Game(int uid, String name, String backgroundImage, String released, float rating, int metacritic, int playtime,String publisher, String developer, String description) {
        this.uid = uid;
        this.name = name;
        this.backgroundImage = backgroundImage;
        this.released = released;
        this.rating = rating;
        this.metacritic = metacritic;
        this.playtime = playtime;
        this.publisher = publisher;
        this.developer = developer;
        this.description = description;
    }

    public Game() {

    }
}

