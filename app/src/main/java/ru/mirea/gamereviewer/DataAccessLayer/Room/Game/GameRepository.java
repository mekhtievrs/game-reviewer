package ru.mirea.gamereviewer.DataAccessLayer.Room.Game;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Database.AppRoomDatabase;
import ru.mirea.gamereviewer.DataAccessLayer.Room.GameWithReviews.GameWithReviews;

public class GameRepository {
    private GameDao gameDao;
    private AppRoomDatabase db;

  //  private LiveData<List<Game>> allGames;

    public GameRepository(Application application) {
        db = AppRoomDatabase.getDatabase(application);
        gameDao = db.gameDao();
        //allGames = gameDao.getAllGames();
//        Log.d(GameRepository.class.toString(), mAllGames.getValue().toString());
//        System.out.println(mAllGames.getValue());
    }

    public LiveData<List<Game>> getAllGames() {
        return gameDao.getAllGames();
    }

    //public LiveData<Game> getGame(String gameName) { return gameDao.getGame(gameName); }

    public LiveData<List<GameWithReviews>> getGame(String gameName) { return gameDao.getGame(gameName); }

    public void insert(Game game) {
        AppRoomDatabase.databaseWriteExecutor.execute(() -> {
            gameDao.insert(game);
        });
    }



}