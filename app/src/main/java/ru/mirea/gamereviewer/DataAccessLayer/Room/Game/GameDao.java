package ru.mirea.gamereviewer.DataAccessLayer.Room.Game;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.GameWithReviews.GameWithReviews;

@Dao
public interface GameDao {

    @Query("SELECT * FROM game_table")
    LiveData<List<Game>> getAllGames();

    @Query("SELECT * FROM game_table WHERE name = :gameName")
    LiveData<List<GameWithReviews>> getGame(String gameName);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Game game);

    @Delete
    void delete(Game game);

}
