package ru.mirea.gamereviewer.DataAccessLayer.Room.UserWithReviews;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;

public class UserWithReviews {
    @Embedded
    public User user;

    @Relation(
            parentColumn = "id",
            entityColumn = "userID"
    )
    public List<Review> reviews;
}
