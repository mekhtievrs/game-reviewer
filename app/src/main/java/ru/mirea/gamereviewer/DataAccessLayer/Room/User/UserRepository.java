package ru.mirea.gamereviewer.DataAccessLayer.Room.User;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Database.AppRoomDatabase;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Library.Library;
import ru.mirea.gamereviewer.DataAccessLayer.Room.UserWithReviews.UserWithReviews;

public class UserRepository {
    private AppRoomDatabase db;
    private UserDao userDao;
    private Library library;


    private static User currentUser;

    public UserRepository(Application application){
        db = AppRoomDatabase.getDatabase(application);
        userDao = db.userDao();
        library = db.library();
    }

    public void saveCurrentUser(String login){
        currentUser = userDao.getUser(login).getValue();
    }

    public void addUser(User user){
        AppRoomDatabase.databaseWriteExecutor.execute( () -> {
            userDao.insert(user);
        });

        currentUser = user;
    }

    public boolean checkUser(String userName){
        LiveData<User> user = this.userDao.getUser(userName);

        if(user.getValue() == null)
            return false;
        else
            return user.getValue().nickName.equals(userName);
    }

    public LiveData<List<User>> getAllUsers() { return this.userDao.getAllUsers(); }

    public static User getCurrentUser() { return currentUser; }

    public LiveData<User> getUser(String userName){
        return this.userDao.getUser(userName);
    }

    public LiveData<List<User>> getUser(int id){
        return this.userDao.getUser(id);
    }

    public LiveData<List<UserWithReviews>> getAllUsersWithReviews(String userName) { return this.library.getUsersReviews(userName); }

    public void setUsersDiscord(String value, int id) {
        AppRoomDatabase.databaseWriteExecutor.execute( () -> {
            this.userDao.updateDiscord(value, id);
        });
    }
}
