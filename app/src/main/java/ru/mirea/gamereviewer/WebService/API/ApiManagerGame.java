package ru.mirea.gamereviewer.WebService.API;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class ApiManagerGame {
    public static void connetionWithUrl(String url){
        //GameObj gameObj = new GameObj();
        try {
            URL apiGamesEndpoint = new URL(url);
            HttpsURLConnection apiGamesConnection = (HttpsURLConnection) apiGamesEndpoint.openConnection();

            if (apiGamesConnection.getResponseCode() == 200) {
                InputStream responseBody = apiGamesConnection.getInputStream();
                InputStreamReader responseBodyReader = new InputStreamReader(responseBody, "UTF-8");
                JsonManagerGame jsonManagerGame = new JsonManagerGame();
                jsonManagerGame.jsonParse(responseBodyReader);
            } else {
                // Error handling code goes here
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

