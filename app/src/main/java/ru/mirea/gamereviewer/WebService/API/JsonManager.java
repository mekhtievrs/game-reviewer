package ru.mirea.gamereviewer.WebService.API;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class JsonManager {
    public String value;
    public String key;

    // private JsonReader jsonReader;

    public JsonManager(String key) {
        this.key = key;
    }

    public JsonManager() {
    }

    public void jsonParse(InputStreamReader responseBodyReader) throws IOException {
        JsonReader jsonReader = new JsonReader(responseBodyReader);
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            key = jsonReader.nextName();
            if (key.equals("results")) {
                readMessagesArray(jsonReader);
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }

    public void readGameList(JsonReader jsonReader) throws IOException {
        String id = null;
        String url = null;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String key = jsonReader.nextName();
            switch (key) {
                case "id":
                    id = jsonReader.nextString();
                    url = "https://api.rawg.io/api/games/" + id + "?key=4a0700d0745f466fbbacedd7e26da9fe";
                    ApiManagerGame.connetionWithUrl(url);
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
    }

    public void readMessagesArray(JsonReader reader) throws IOException {

        reader.beginArray();
        while (reader.hasNext()) {
            readGameList(reader);
        }
        reader.endArray();
    }

}

