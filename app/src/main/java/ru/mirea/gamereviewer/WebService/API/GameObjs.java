package ru.mirea.gamereviewer.WebService.API;

import java.util.ArrayList;
import java.util.List;

public class GameObjs {
    public static List<GameObj> gameObjsList = new ArrayList<>();

    public void add(int id, String name, String backgroundImage, String released, float rating, int metacritic, int playtime, String publisher, String developer, String description ) {
        gameObjsList.add(
                new GameObj(
                        id,
                        name,
                        backgroundImage,
                        released,
                        rating,
                        metacritic,
                        playtime,
                        publisher,
                        developer,
                        description
                )
        );
    }
}
