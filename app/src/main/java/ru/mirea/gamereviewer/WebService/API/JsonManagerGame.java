package ru.mirea.gamereviewer.WebService.API;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class JsonManagerGame {
    public String value;

    // private JsonReader jsonReader;


    public JsonManagerGame() {
    }

    public void jsonParse(InputStreamReader responseBodyReader) throws IOException {
        JsonReader jsonReader = new JsonReader(responseBodyReader);
        GameObjs gameObjs = new GameObjs();
        int id = -1;
        String name = null;
        String backgroundImage = null;
        String released = null;
        String publisher = null;
        String developer = null;
        String description = null;
        float rating = -1;
        int metacritic = -1;
        int playtime = -1;

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String key = jsonReader.nextName();
            switch (key) {
                case "id":
                    id = jsonReader.nextInt();
                    break;
                case "name":
                    name = jsonReader.nextString();
                    break;
                case "background_image":
                    backgroundImage = jsonReader.nextString();
                    break;
                case "released":
                    released = jsonReader.nextString();
                    break;
                case "rating":
                    rating = (float) jsonReader.nextDouble();
                    break;
                case "metacritic":
                    metacritic = jsonReader.nextInt();
                    break;
                case "playtime":
                    playtime = jsonReader.nextInt();
                    break;
                case "developers":
                    developer = readMessagesArray(jsonReader,key);
                    break;
                case "publishers":
                    publisher = readMessagesArray(jsonReader,key);
                    break;
                case "description_raw":
                    description = jsonReader.nextString();
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
            if (
                    name != null &&
                            backgroundImage != null &&
                            developer != null &&
                            rating != -1 &&
                            metacritic != -1 &&
                            playtime != -1 &&
                            id != -1 &&
                            released != null &&
                            publisher != null &&
                            description != null
            ) {
                gameObjs.add(
                        id,
                        name,
                        backgroundImage,
                        released,
                        rating,
                        metacritic,
                        playtime,
                        publisher,
                        developer,
                        description
                );
                name = null;
                backgroundImage = null;
                released = null;
                developer = null;
                publisher = null;
                description = null;
                rating = -1;
                metacritic = -1;
                playtime = -1;
            }
        }
        jsonReader.endObject();
    }

    public String readDevelopers(JsonReader jsonReader) throws IOException {
        String name = null;
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String key = jsonReader.nextName();
            switch (key) {
                case "name":
                    name = jsonReader.nextString();
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }

        }
        jsonReader.endObject();
        return name;
    }

    public String readPublishers(JsonReader jsonReader) throws IOException {
        String name = null;

        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String key = jsonReader.nextName();
            switch (key) {
                case "name":
                    name = jsonReader.nextString();
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }

        }
        jsonReader.endObject();
        return name;
    }

    public String readMessagesArray(JsonReader reader, String key) throws IOException {
        String name = null;
        reader.beginArray();
        switch (key){
            case "developers":
                while (reader.hasNext()) {
                   name = readDevelopers(reader);
                }
            case "publishers":
                while (reader.hasNext()) {
                   name = readPublishers(reader);
                }
        }

        reader.endArray();
        return name;
    }

}
