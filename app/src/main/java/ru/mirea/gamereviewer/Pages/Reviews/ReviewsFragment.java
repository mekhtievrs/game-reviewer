package ru.mirea.gamereviewer.Pages.Reviews;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.List;

import ru.mirea.gamereviewer.Adapters.ReviewItemsAdapter;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.DataAccessLayer.Room.UserWithReviews.UserWithReviews;
import ru.mirea.gamereviewer.R;

public class ReviewsFragment extends Fragment {
    private ReviewsViewModel reviewsViewModel;
    private CollapsingToolbarLayout _collapsingToolbar;
    private AppBarLayout appBarLayout;
    private NavController navController;

    private TextView userName;
    private TextView userNickName;
    private Button makeNewReview;

    private ReviewItemsAdapter reviewItemsAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Говорим фрагменту, чтобы он разворачивал layout fragment_reviews и вернул нам его как View
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);

        // Инициализация контроллера навигации
        initializeNavigation();

        initializeTextViews(view);

        // Анимация свайпа вверх
        initializeCollapsingToolbar(view);

        // Инициализация Recycler View, который будет содержать карточки обзоров на игры
        initializeReviewsRecyclerView(view);

        // Инициализация ReviewsViewModel
        initializeViewModel();

        reviewsViewModel.getUsersReviews().observe(getViewLifecycleOwner(), new Observer<List<UserWithReviews>>() {
            @Override
            public void onChanged(List<UserWithReviews> userWithReviews) {
                if (userWithReviews != null){
                    UserWithReviews test = userWithReviews.get(0);

                    reviewItemsAdapter.setGames(test.reviews);

                    Log.i("Check", test.reviews.toString());
                }
                else{
                    int a = 1;
                }
            }
        });

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Описываем функционал кнопки при нажатии
        setMakeReviewButtonClickListener();
    }

    private void initializeTextViews(View view){
        userName = view.findViewById(R.id.user_name_review_text);
        userNickName = view.findViewById(R.id.user_nick_name_review_text);
        makeNewReview = view.findViewById(R.id.make_new_review_button);
    }

    private void initializeViewModel(){
        reviewsViewModel = new ViewModelProvider(this).get(ReviewsViewModel.class);

        User user = reviewsViewModel.getCurrentUser();

        if(user != null){
            reviewsViewModel.getUser(user.userName).observe(getViewLifecycleOwner(), new Observer<User>() {
                @Override
                public void onChanged(User user) {
                    if(user != null){
                        userName.setText(user.userName);
                        userNickName.setText(String.valueOf(user.id));


                        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
                            boolean isShow = false;
                            int scrollRange = -1;

                            @Override
                            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                                if (scrollRange == -1) {
                                    scrollRange = appBarLayout.getTotalScrollRange();
                                }
                                if (scrollRange + verticalOffset == 0) {
                                    //when collapsingToolbar at that time display actionbar title
                                    _collapsingToolbar.setTitle(
                                            user.userName +
                                            "                                                       " +
                                            user.nickName
                                    );
                                    isShow = true;
                                } else if (isShow) {
                                    //careful there must a space between double quote otherwise it dose't work
                                    _collapsingToolbar.setTitle(" ");
                                    isShow = false;
                                }
                            }
                        });
                    }
                    else{
                        int a = 1;
                    }
                }
            });
        }
    }

    private void initializeNavigation(){
        navController = Navigation.findNavController(getActivity(), R.id.main_container);
    }

    /**
     * Инициализация Recycler View, который будет содержать карточки обзоров на игры
     * */
    private void initializeReviewsRecyclerView(View view){
        // Находим наш Recycler View по id
        RecyclerView recyclerViewReviews = view.findViewById(R.id.review_recycler_view);

        // Говорим ему, что мы знаем размер заранее (статический размер)
        recyclerViewReviews.setHasFixedSize(true);

        // Используем отоборажение в виде списка
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerViewReviews.setLayoutManager(linearLayoutManager);

        // Для нашего Recycler View устанавливаем логику (Adapter)
        reviewItemsAdapter = new ReviewItemsAdapter(10);

        reviewItemsAdapter.setNavigationController(navController);

        recyclerViewReviews.setAdapter(reviewItemsAdapter);
    }


    /**
     * Инициализация тулбара (анимация свайпа вверх)*/
    private void initializeCollapsingToolbar(View view){
        _collapsingToolbar = view.findViewById(R.id.collapsing_toolbar);
        _collapsingToolbar.setCollapsedTitleTextAppearance(R.style.TextAppearance_GameReviewer_Title_Collapsed);
        _collapsingToolbar.setExpandedTitleTextAppearance(R.style.TextAppearance_GameReviewer_Title_Expanded);

        // This is the most important when you are putting custom TextView in CollapsingToolbar
        _collapsingToolbar.setTitle(" ");

        appBarLayout = view.findViewById(R.id.reviews_app_bar);
    }

    private void setMakeReviewButtonClickListener(){
        makeNewReview.setOnClickListener(v -> {
            try {
                navController.navigate(R.id.action_navigation_reviews_to_navigation_review_creation);
            }
            catch (Exception ignored){

            }
        });
    }
}