package ru.mirea.gamereviewer.Pages.Games;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.mirea.gamereviewer.Adapters.GameItemsAdapter;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.R;
import ru.mirea.gamereviewer.WebService.API.GameObj;
import ru.mirea.gamereviewer.WebService.API.GameObjs;

public class AllGamesFragment extends Fragment {
    public AllGamesViewModel allGamesViewModel;
    public List<Game> games = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public AllGamesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Говорим фрагменту, чтобы он разворачивал layout fragment_all_games и вернул нам его как View
        View view = inflater.inflate(R.layout.fragment_all_games, container, false);

        // Инициализация Recycler View, который будет содержать карточки игр
        GameItemsAdapter gameItemAdapter = initializeGamesRecyclerView(view);

        allGamesViewModel = new ViewModelProvider(this).get(AllGamesViewModel.class);
        //add game
        for (GameObj item : GameObjs.gameObjsList) {
            allGamesViewModel.insert(
                    new Game(
                            item.getId(),
                            item.getName(),
                            item.getBackgroundImage(),
                            item.getReleased(),
                            item.getRating(),
                            item.getMetacritic(),
                            item.getPlaytime(),
                            item.getPublisher(),
                            item.getDeveloper(),
                            item.getDescription()

                    )
            );
        }


        allGamesViewModel.getAll().observe(getViewLifecycleOwner(), new Observer<List<Game>>() {
            @Override
            public void onChanged(@Nullable List<Game> games) {
                gameItemAdapter.setGames(games);
            }
        });

        return view;
    }

    /**
     * Инициализация Recycler View, который будет содержать карточки игр
     */
    private GameItemsAdapter initializeGamesRecyclerView(View view) {
        // Находим наш Recycler View по id
        RecyclerView recyclerViewGames = view.findViewById(R.id.game_recycler_view);

        // Говорим ему, что мы знаем размер заранее (статический размер)
        recyclerViewGames.setHasFixedSize(true);

        // Используем отоборажение в виде списка
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        recyclerViewGames.setLayoutManager(linearLayoutManager);

        // Для нашего Recycler View устанавливаем логику (Adapter)
        GameItemsAdapter gameItemAdapter = new GameItemsAdapter();

        /*gameItemAdapter.setOnItemClickListener(new ClickListener() {
            @Override
            public void onItemClick() {
                try {
                    // TODO: обработать нажатие
                    NavController _navController = Navigation.findNavController(getActivity(), R.id.main_container);
                    _navController.navigate(R.id.action_navigation_all_games_to_navigation_game_profile);
                    //((MainActivity) getActivity()).openGameProfile();
                }
                catch(NullPointerException e){
                    // TODO: обработать исключение
                }
            }
        });*/

        gameItemAdapter.setNavigationController(Navigation.findNavController(getActivity(), R.id.main_container));

        recyclerViewGames.setAdapter(gameItemAdapter);
        return gameItemAdapter;
    }
}

