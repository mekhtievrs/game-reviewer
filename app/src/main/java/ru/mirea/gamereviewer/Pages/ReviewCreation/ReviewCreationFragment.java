package ru.mirea.gamereviewer.Pages.ReviewCreation;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.ArrayList;
import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.DataAccessLayer.Room.GameWithReviews.GameWithReviews;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.R;

public class ReviewCreationFragment extends Fragment {
    private ReviewCreationViewModel reviewCreationViewModel;

    private Spinner spinner;
    private ArrayAdapter arrayAdapter;
    private ArrayList<String> gamesNames;

    private String selectedGame;

    private EditText gameName;
    private EditText gameRating;
    private EditText gameDescription;
    private Button addReviewButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review_creation, container, false);

        initializeViews(view);

        initializeViewModel();

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        addReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = gameDescription.getText().toString();
                float rating = Float.parseFloat(gameRating.getText().toString());

                User user = reviewCreationViewModel.getCurrentUser();

                reviewCreationViewModel.getUser(user.userName).observe(getViewLifecycleOwner(), new Observer<User>() {
                    @Override
                    public void onChanged(User user) {
                        reviewCreationViewModel.getGame(selectedGame).observe(getViewLifecycleOwner(), new Observer<List<GameWithReviews>>() {
                            @Override
                            public void onChanged(List<GameWithReviews> gameWithReviews) {
                                GameWithReviews game = gameWithReviews.get(0);
                                reviewCreationViewModel.addReview(new Review(description, rating, game.game.name, game.game.backgroundImage, user.id, game.game.id));
                                getActivity().onBackPressed();
                            }
                        });
                    }
                });


            }
        });

        initializeSpinner();
    }

    private void initializeViews(View view){
        gameName = view.findViewById(R.id.review_game_name_edit_text);
        gameRating = view.findViewById(R.id.review_game_rating_edit_text);
        gameDescription = view.findViewById(R.id.review_game_description_edit_text);

        addReviewButton = view.findViewById(R.id.add_review_button);

        spinner = view.findViewById(R.id.review_games_spinner);
    }

    private void initializeViewModel(){
        reviewCreationViewModel = new ViewModelProvider(this).get(ReviewCreationViewModel.class);
    }

    private void initializeSpinner(){
        reviewCreationViewModel.getGames().observe(getViewLifecycleOwner(), new Observer<List<Game>>() {
            @Override
            public void onChanged(List<Game> games) {
                gamesNames = new ArrayList<>();

                for(int i = 0; i < games.size(); i++){
                    gamesNames.add(games.get(i).name);
                }

                arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, gamesNames);

                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                spinner.setAdapter(arrayAdapter);

                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        String item = parent.getItemAtPosition(position).toString();
                        selectedGame = item;
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        //String item = parent.getItemAtPosition(position);
                    }
                });
            }
        });

        /*spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position);
                sele
            }


        });*/


    }
}
