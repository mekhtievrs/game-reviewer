package ru.mirea.gamereviewer.Pages.LogIn;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LogInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LogInFragment extends Fragment {
    private LogInViewModel logInViewModel;
    private NavController navController;

    private EditText login;
    private EditText password;
    private Button signInButton;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public LogInFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment LogIn.
     */
    // TODO: Rename and change types and number of parameters
    public static LogInFragment newInstance(String param1, String param2) {
        LogInFragment fragment = new LogInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_log_in, container, false);

        // Инициализация полей ввода логина и пароля
        initializeEditTexts(view);

        // Инициализация кнопки SignIn ("войти")
        initializeSignInButton(view);


        logInViewModel = new ViewModelProvider(this).get(LogInViewModel.class);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Инициализация контроллера навигации
        initializeNavigationController();
    }

    private void initializeSignInButton(View view){
        signInButton = view.findViewById(R.id.sign_in_button);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loginText = login.getText().toString();

                if(!loginText.equals("") && !loginText.equals(" ")){
                    boolean userExists = logInViewModel.checkUser(loginText);

                    if(userExists){
                        logInViewModel.saveCurrentUser(loginText);
                    }
                    else{
                        User user = new User(loginText, "TestNick");
                        logInViewModel.addUser(user);
                    }

                    if(navController != null){
                        NavigationView sideNavigationView = getActivity().findViewById(R.id.side_navigation_view);
                        sideNavigationView.getMenu().setGroupEnabled(R.id.group_side_menu, true);
                        sideNavigationView.getHeaderView(0).setEnabled(true);

                        BottomNavigationView bottomNavigationView = getActivity().findViewById(R.id.bottom_navigation_view);
                        bottomNavigationView.getMenu().setGroupEnabled(R.id.group_bottom_menu, true);

                        navController.navigate(R.id.action_navigation_log_in_to_navigation_profile);
                    }

                }
            }
        });
    }

    private void initializeNavigationController(){
        navController = Navigation.findNavController(getActivity(), R.id.main_container);
    }

    private void initializeEditTexts(View view){
        login = view.findViewById(R.id.email_edit);
        password = view.findViewById(R.id.password_edit);
    }

}