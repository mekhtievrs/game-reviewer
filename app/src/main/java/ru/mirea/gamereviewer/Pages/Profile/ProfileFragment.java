package ru.mirea.gamereviewer.Pages.Profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.R;

public class ProfileFragment extends Fragment {
    private ProfileViewModel profileViewModel;
    private TextView userName;
    private TextView userNickName;
    private TextView userSteam;
    private TextView userDiscord;
    private EditText steamEdit;
    private EditText discordEdit;
    private Button addSteamButton;
    private Button addDiscordButton;
    private Button confirmButton;
    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        initializeViews(view);

        profileViewModel = new ViewModelProvider(this).get(ProfileViewModel.class);

        user = profileViewModel.getCurrentUser();

        if(user != null){
            profileViewModel.getUser(user.userName).observe(getViewLifecycleOwner(), new Observer<User>() {
                @Override
                public void onChanged(User user) {
                    if(user != null){
                        userName.setText(user.userName);
                        userNickName.setText("ID: ".concat(String.valueOf(user.id)));

                        /*if (user.steam == null || user.steam.isEmpty())
                            addSteamButton.setVisibility(View.VISIBLE);
                        else
                            userSteam.setText(user.steam);

                        if(user.discord == null || user.steam.isEmpty())
                            addDiscordButton.setVisibility(View.VISIBLE);
                        else
                            userDiscord.setText(user.discord);*/
                    }
                }
            });

        }
        else{
            userName.setText("Не сработало");
            userNickName.setText("Не сработало");
        }

        setOnButtonClickListeners();

        return view;
    }

    private void initializeViews(View view){
        userName = view.findViewById(R.id.user_name_text);
        userNickName = view.findViewById(R.id.user_nick_name_text);
        userSteam = view.findViewById(R.id.steam_text);
        userDiscord = view.findViewById(R.id.discord_text);

        steamEdit = view.findViewById(R.id.steam_edit_text);
        discordEdit = view.findViewById(R.id.discord_edit_text);

        addSteamButton = view.findViewById(R.id.add_steam_button);
        addDiscordButton = view.findViewById(R.id.add_discord_button);
        confirmButton = view.findViewById(R.id.user_profile_confirm_button);
    }

    private void setOnButtonClickListeners(){
        addSteamButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSteamButton.setVisibility(View.GONE);
                steamEdit.setVisibility(View.VISIBLE);
                confirmButton.setVisibility(View.VISIBLE);
            }
        });

        addDiscordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addDiscordButton.setVisibility(View.GONE);
                discordEdit.setVisibility(View.VISIBLE);
                confirmButton.setVisibility(View.VISIBLE);
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String steam = steamEdit.getText().toString();
                String discord = discordEdit.getText().toString();

                steamEdit.setVisibility(View.GONE);
                discordEdit.setVisibility(View.GONE);

                confirmButton.setVisibility(View.GONE);

                user = profileViewModel.getCurrentUser();
                profileViewModel.getUser(user.userName).observe(getViewLifecycleOwner(), new Observer<User>() {
                    @Override
                    public void onChanged(User user) {
                        profileViewModel.setUsersDiscord(discord, user.id);
                    }
                });
            }
        });
    }
}
