package ru.mirea.gamereviewer.Pages.LogIn;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.UserRepository;

public class LogInViewModel extends AndroidViewModel {
    private LiveData<List<User>> allUsers;
    private final UserRepository userRepository;

    public LogInViewModel(Application application) {
        super(application);

        userRepository = new UserRepository(application);
    }

    public LiveData<List<User>> getAllUsers() { return this.userRepository.getAllUsers(); }

    public void saveCurrentUser(String login){
        this.userRepository.saveCurrentUser(login);
    }

    public void addUser(User user){
        this.userRepository.addUser(user);
    }

    public boolean checkUser(String login){
        return userRepository.checkUser(login);
    }

    public User getUser(String login){ return userRepository.getUser(login).getValue(); }
}
