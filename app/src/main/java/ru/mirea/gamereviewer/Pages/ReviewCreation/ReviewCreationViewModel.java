package ru.mirea.gamereviewer.Pages.ReviewCreation;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.GameRepository;
import ru.mirea.gamereviewer.DataAccessLayer.Room.GameWithReviews.GameWithReviews;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.ReviewRepository;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.UserRepository;

public class ReviewCreationViewModel extends AndroidViewModel {
    private final ReviewRepository reviewRepository;
    private final UserRepository userRepository;
    private final GameRepository gameRepository;


    public ReviewCreationViewModel(Application application){
        super(application);

        userRepository = new UserRepository(application);
        reviewRepository = new ReviewRepository(application);
        gameRepository = new GameRepository(application);
    }

    public void addReview(Review review){
        reviewRepository.addReview(review);
    }

    public User getCurrentUser(){
        return UserRepository.getCurrentUser();
    }

    public LiveData<User> getUser(String userName) { return userRepository.getUser(userName); }

    public LiveData<List<Game>> getGames() { return gameRepository.getAllGames(); }

    public LiveData<List<GameWithReviews>> getGame(String gameName) { return gameRepository.getGame(gameName); }
 }
