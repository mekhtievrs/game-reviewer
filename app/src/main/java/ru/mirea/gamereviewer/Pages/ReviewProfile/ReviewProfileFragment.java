package ru.mirea.gamereviewer.Pages.ReviewProfile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;
import ru.mirea.gamereviewer.R;

public class ReviewProfileFragment extends Fragment {
    private TextView gameReviewDescription;
    private RatingBar gameReviewRating;
    private TextView gameReviewGame;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_review_profile, container, false);

        initializeViews(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setReviewData();
    }

    private void initializeViews(View view){
        gameReviewGame = view.findViewById(R.id.review_profile_game_name);
        gameReviewRating = view.findViewById(R.id.review_profile_game_rating);
        gameReviewDescription = view.findViewById(R.id.review_profile_game_description);
    }

    private void setReviewData(){
        Bundle bundle = getArguments();

        if (bundle != null) {
            List<Review> reviews = (List<Review>)bundle.getSerializable("REVIEW_OBJECT");

            if(reviews != null){
                Review review = reviews.get(0);
                gameReviewGame.setText(review.gameName);
                gameReviewRating.setRating(review.rating);
                gameReviewDescription.setText(review.description);
            }
        }
    }
}
