package ru.mirea.gamereviewer.Pages.GameProfile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GameProfileViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public GameProfileViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
