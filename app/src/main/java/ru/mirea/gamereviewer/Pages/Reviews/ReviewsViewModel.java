package ru.mirea.gamereviewer.Pages.Reviews;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.ReviewRepository;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.UserRepository;
import ru.mirea.gamereviewer.DataAccessLayer.Room.UserWithReviews.UserWithReviews;

public class ReviewsViewModel extends AndroidViewModel {
    private final UserRepository userRepository;
    private final ReviewRepository reviewRepository;

    public ReviewsViewModel(Application application) {
        super(application);

        userRepository = new UserRepository(application);
        reviewRepository = new ReviewRepository(application);
    }

    public User getCurrentUser(){
        return UserRepository.getCurrentUser();
    }

    public LiveData<List<User>> getUser(int id){ return userRepository.getUser(id); }

    public LiveData<User> getUser(String userName){ return userRepository.getUser(userName); }

    public LiveData<List<User>> getAllUsers() { return userRepository.getAllUsers(); }

    public LiveData<List<UserWithReviews>> getUsersReviews() {
        User user = getCurrentUser();

        return userRepository.getAllUsersWithReviews(user.userName);
    }

    public void addReview(Review review){
        reviewRepository.addReview(review);
    }
}
