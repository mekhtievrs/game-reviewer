package ru.mirea.gamereviewer.Pages.GameProfile;

import android.graphics.text.LineBreaker;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GameProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GameProfileFragment extends Fragment {

    private TextView gameName;
    private ImageView gameImage;
    private TextView gameMetacritic;
    private TextView gameDescription;
    private TextView gameDeveloper;
    private TextView gamePublisher;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARGUMENT = "GAME_OBJECT";
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String GameName = "";
    private String GameImage = "";
    private String GameDescription = "";
    private String GameDeveloper = "";
    private String GamePublisher = "";
    private int GameMetacritic = 0;

    public GameProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GameProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GameProfileFragment newInstance(String param1, String param2) {
        GameProfileFragment fragment = new GameProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_game_profile, container, false);

        initializeViews(view);

        Bundle bundle = getArguments();

        if (bundle != null) {
            List<Game> games = (List<Game>)bundle.getSerializable("GAME_OBJECT");

            if(games != null){
                Game game = games.get(0);
                GameName = game.name;
                GameImage = game.backgroundImage;
                GameMetacritic = game.metacritic;
                GameDescription = game.description;
                GameDeveloper = game.developer;
                GamePublisher = game.publisher;
            }
        }

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Инициализация скроллинга для текста
        initializeScrollingText(view);

        gameName.setText(GameName);
        Picasso.get().load(GameImage).into(gameImage);
        gameDescription.setText(String.valueOf(GameDescription));
        gameMetacritic.setText(String.valueOf(GameMetacritic));
        gameDeveloper.setText(String.valueOf(GameDeveloper));
        gamePublisher.setText(String.valueOf(GamePublisher));
    }

    /**
     * Инициализация скроллинга для текста*/
    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void initializeScrollingText(View view){
        TextView textView = view.findViewById(R.id.game_about_text);
        textView.setMovementMethod(new ScrollingMovementMethod());
        textView.setJustificationMode(LineBreaker.JUSTIFICATION_MODE_INTER_WORD);
    }

    private void initializeViews(View view){
        gameName = view.findViewById(R.id.game_name_label);
        gameImage = view.findViewById(R.id.game_profile_game_image);
        gameDescription = view.findViewById(R.id.game_about_text);
        gameMetacritic = view.findViewById(R.id.game_tags);
        gameDeveloper = view.findViewById(R.id.game_developers);
        gamePublisher = view.findViewById(R.id.game_publishers);
    }
}