package ru.mirea.gamereviewer.Pages.Profile;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.User.User;
import ru.mirea.gamereviewer.DataAccessLayer.Room.User.UserRepository;

public class ProfileViewModel extends AndroidViewModel {
    private final UserRepository userRepository;

    public ProfileViewModel(Application application) {
        super(application);

        userRepository = new UserRepository(application);
    }

    public User getCurrentUser(){
        return UserRepository.getCurrentUser();
    }

    public LiveData<List<User>> getUser(int id){ return userRepository.getUser(id); }

    public LiveData<User> getUser(String userName){ return userRepository.getUser(userName); }

    public LiveData<List<User>> getAllUsers() { return userRepository.getAllUsers(); }

    public void setUsersDiscord(String discord, int id) { userRepository.setUsersDiscord(discord, id);}
}
