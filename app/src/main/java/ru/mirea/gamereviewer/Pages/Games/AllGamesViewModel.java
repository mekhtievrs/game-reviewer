package ru.mirea.gamereviewer.Pages.Games;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.GameRepository;

public class AllGamesViewModel extends AndroidViewModel {
    private LiveData<List<Game>> allGames;
    private GameRepository mRepository;


    public AllGamesViewModel(Application application) {
        super(application);
        mRepository = new GameRepository(application);
        allGames = mRepository.getAllGames();

        System.out.println(allGames.getValue());
    }

    public LiveData<List<Game>> getAll() {
        return allGames;
    }

    public void insert(Game game) {
        mRepository.insert(game);
    }

}

