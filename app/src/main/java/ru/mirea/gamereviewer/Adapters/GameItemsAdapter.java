package ru.mirea.gamereviewer.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ru.mirea.gamereviewer.DataAccessLayer.Room.Game.Game;
import ru.mirea.gamereviewer.Helpers.Listeners.ClickListener;
import ru.mirea.gamereviewer.R;


public class GameItemsAdapter extends RecyclerView.Adapter<GameItemsAdapter.GameItemViewHolder> {
    private NavController navController;
    private static int viewHolderCount;
    private final int _amountOfItems;
    public List<Game> games = new ArrayList<>();

    // TODO: сделать нормальный listener через делегаты
    private ClickListener _clickListener;

    public GameItemsAdapter() {
        this._amountOfItems = 1;
        viewHolderCount = 0;
    }

    public GameItemsAdapter(int amountOfItems) {
        this._amountOfItems = amountOfItems;
        viewHolderCount = 0;
    }

    @NonNull
    @Override
    public GameItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForGameCardItem = R.layout.game_card_item;
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(layoutIdForGameCardItem, parent, false);

        GameItemViewHolder gameItemViewHolder = new GameItemViewHolder(view);
        gameItemViewHolder.gameCardRating.setText(String.valueOf(viewHolderCount));
        viewHolderCount++;

        return gameItemViewHolder;
    }

    public void setGames(List<Game> games) {
        this.games = games;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull GameItemViewHolder holder, int position) {
        Game currentGame = games.get(position);
        holder.bind(currentGame);

        // Назначаем событию слушателя
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Bundle bundle = new Bundle();
                    String key = "GAME_OBJECT";
                    List<Game> games = new ArrayList<Game>();
                    games.add(currentGame);
                    bundle.putSerializable(key, (Serializable) games);
                    navController.navigate(R.id.action_navigation_all_games_to_navigation_game_profile, bundle);
                } catch (Exception e) {

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (games != null)
            return games.size();
        else return 0;
    }

    public void setOnItemClickListener(ClickListener userClickListener) {
        this._clickListener = userClickListener;
    }

    public void setNavigationController(NavController navController) {
        this.navController = navController;
    }


    /**
     * Наш класс, который обрабатывает объекты списка (ViewHolder'ы)
     */
    class GameItemViewHolder extends RecyclerView.ViewHolder {

        TextView gameCardName;
        TextView gameCardRating;
        CardView cardView;
        ImageView gameCardImage;

        public GameItemViewHolder(@NonNull View itemView) {
            super(itemView);
            gameCardName = itemView.findViewById(R.id.game_card_name);
            gameCardRating = itemView.findViewById(R.id.game_card_rating);
            cardView = itemView.findViewById(R.id.game_card_view);
            gameCardImage = itemView.findViewById(R.id.game_card_image);
        }

        void bind(Game currentGame) {
            gameCardName.setText(currentGame.name);
            gameCardRating.setText(String.valueOf(currentGame.metacritic));
            Picasso.get().load(currentGame.backgroundImage).into(gameCardImage);
        }
    }
}



