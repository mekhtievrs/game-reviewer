package ru.mirea.gamereviewer.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.mirea.gamereviewer.DataAccessLayer.Room.Review.Review;
import ru.mirea.gamereviewer.Helpers.Listeners.ClickListener;
import ru.mirea.gamereviewer.R;

public class ReviewItemsAdapter extends RecyclerView.Adapter<ReviewItemsAdapter.ReviewItemViewHolder>{
    private static int viewHolderCount;
    private final int amountOfItems;
    private NavController navController;

    private List<Review> reviews;


    // TODO: сделать нормальный listener через делегаты
    private ClickListener clickListener;

    public ReviewItemsAdapter(){
        this.amountOfItems = 10;
        viewHolderCount = 0;
    }

    public ReviewItemsAdapter(int amountOfItems){
        this.amountOfItems = amountOfItems;
        viewHolderCount = 0;
    }

    @SuppressLint("SetTextI18n")
    @NonNull
    @Override
    public ReviewItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForReviewCardItem = R.layout.review_card_item;
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(layoutIdForReviewCardItem, parent, false);

        ReviewItemViewHolder reviewItemViewHolder = new ReviewItemViewHolder(view);
        reviewItemViewHolder.reviewDescription.setText("ViewHolderCount = " + viewHolderCount);
        viewHolderCount++;

        return reviewItemViewHolder;
    }

    public void setGames(List<Review> reviews){
        this.reviews = reviews;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewItemsAdapter.ReviewItemViewHolder holder, int position) {
        Review currentReview = reviews.get(position);

        holder.bind(currentReview);

        // Назначаем событию слушателя
        holder.cardViewReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    Bundle bundle = new Bundle();
                    String key = "REVIEW_OBJECT";
                    List<Review> reviews = new ArrayList<Review>();
                    reviews.add(currentReview);
                    bundle.putSerializable(key, (Serializable) reviews);
                    navController.navigate(R.id.action_navigation_reviews_to_navigation_review_profile, bundle);
                }
                catch (Exception e){

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        if (reviews != null)
            return reviews.size();
        else return 0;
    }

    public void setOnItemClickListener(ClickListener userClickListener){
        this.clickListener = userClickListener;
    }

    public void setNavigationController(NavController navController){
        this.navController = navController;
    }


    /**
     * Наш класс, который обрабатывает объекты списка (ViewHolder'ы)*/
    class ReviewItemViewHolder extends RecyclerView.ViewHolder{

        CardView cardViewReview;
        TextView reviewGameName;
        TextView reviewDescription;
        RatingBar reviewCardRating;
        CircleImageView circleImageView;

        public ReviewItemViewHolder(@NonNull View itemView) {
            super(itemView);

            cardViewReview = itemView.findViewById(R.id.review_card_view);
            reviewGameName = itemView.findViewById(R.id.review_card_game_name);
            reviewCardRating = itemView.findViewById(R.id.review_card_game_rating);

            reviewDescription = itemView.findViewById(R.id.review_text);

            circleImageView = itemView.findViewById(R.id.review_card_game_image);
        }


        void bind(Review currentReview){
            reviewGameName.setText(currentReview.gameName);
            reviewCardRating.setRating(currentReview.rating);
            reviewDescription.setText(currentReview.description);

            Picasso.get().load(currentReview.gameImage).into(circleImageView);
        }
    }
}
