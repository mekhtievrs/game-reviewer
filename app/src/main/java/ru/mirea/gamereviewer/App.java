package ru.mirea.gamereviewer;

import android.app.Application;

//TODO: выяснить, нужен ли этот класс, и в случае чего удалить его (и этот файл)
public class App extends Application {
    public static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }
}
