package ru.mirea.gamereviewer.Helpers.Enums;

public enum Fragments {
    LogIn (0),
    Profile (1),
    GameProfile (2),
    Reviews (3),
    AllGames(4);

    private final int fragmentCode;

    Fragments(int fragmentCode) {
        this.fragmentCode = fragmentCode;
    }

    public int getFragmentCode() {
        return this.fragmentCode;
    }

}
