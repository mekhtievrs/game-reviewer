package ru.mirea.gamereviewer.Helpers.Listeners;

public interface ClickListener {
    void onItemClick();
}
