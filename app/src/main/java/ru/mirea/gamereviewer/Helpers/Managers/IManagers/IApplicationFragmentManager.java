package ru.mirea.gamereviewer.Helpers.Managers.IManagers;

import androidx.annotation.NonNull;

import ru.mirea.gamereviewer.Helpers.Enums.Fragments;

public interface IApplicationFragmentManager {

    /** Устанавливает фрагмент, если вы сохранили id контейнера
     * @param fragment Тип фрагмента*/
    void setFragment(@NonNull Fragments fragment);

    /** Устанавливает фрагмент
     * @param fragment Тип фрагмента
     * @param containerID Номер контейнера*/
    void setFragment(@NonNull Fragments fragment, int containerID);

    /** Устанавливает фрагмент
     * @param fragmentNumber Номер фрагмента
     * @param containerID Номер контейнера*/
    void setFragment(int fragmentNumber, int containerID);


    /** Устанавливает фрагмент, если вы сохранили id контейнера
     * @param containerID Номер контейнера*/
    void setDefaultFragment(int containerID);

    /** Устанавливает дефолтный фрагмент, если вы сохранили id контейнера*/
    void setDefaultFragment();


    /** Сохраняет id контейнера
     * @param containerID Номер контейнера
     * @return IApplicationFragmentManager object (this)
     * @version 1.0*/
    IApplicationFragmentManager saveContainer(int containerID);
}
