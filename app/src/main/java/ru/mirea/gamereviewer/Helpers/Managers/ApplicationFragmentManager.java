package ru.mirea.gamereviewer.Helpers.Managers;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import ru.mirea.gamereviewer.Helpers.Enums.Fragments;
import ru.mirea.gamereviewer.Helpers.Managers.IManagers.IApplicationFragmentManager;
import ru.mirea.gamereviewer.Pages.Games.AllGamesFragment;
import ru.mirea.gamereviewer.Pages.GameProfile.GameProfileFragment;
import ru.mirea.gamereviewer.Pages.LogIn.LogInFragment;
import ru.mirea.gamereviewer.Pages.Profile.ProfileFragment;
import ru.mirea.gamereviewer.Pages.Reviews.ReviewsFragment;


// TODO: перевести класс на статическое использование

public class ApplicationFragmentManager implements IApplicationFragmentManager {

    static Fragments _currentFragment = Fragments.LogIn;

    Fragment _fragment = null;
    FragmentManager _fragmentManager;

    private static int _containerID;
    private static boolean _rememberedContainer;

    public ApplicationFragmentManager(@NonNull FragmentManager supportFragmentManager)
    {
        this._fragmentManager = supportFragmentManager;
        _containerID = 0;
        _rememberedContainer = false;
    }

    public void setFragment(@NonNull Fragments fragment){

        if(!_rememberedContainer)
            return;

        switch (fragment){
            case LogIn:
                this._fragment = new LogInFragment();
                break;
            case Reviews:
                this._fragment = new ReviewsFragment();
                break;
            case GameProfile:
                this._fragment = new GameProfileFragment();
                break;
            case AllGames:
                this._fragment = new AllGamesFragment();
                break;
            case Profile:
            default:
                this._fragment = new ProfileFragment();
                break;
        }

         this._fragmentManager.beginTransaction().replace(_containerID, this._fragment).commit();
         CurrentFragment(fragment);
    }

    public void setFragment(Fragments fragment, int containerID){
        switch (fragment){
            case LogIn:
                this._fragment = new LogInFragment();
                break;
            case Reviews:
                this._fragment = new ReviewsFragment();
                break;
            case GameProfile:
                this._fragment = new GameProfileFragment();
                break;
            case AllGames:
                this._fragment = new AllGamesFragment();
                break;
            case Profile:
            default:
                this._fragment = new ProfileFragment();
                break;
        }
        CurrentFragment(fragment);
        ContainerID(containerID);
        this._fragmentManager.beginTransaction().replace(_containerID, this._fragment).commit();
    }

    public void setFragment(int fragmentNumber, int containerID){
        switch (fragmentNumber){
            case 0:
                CurrentFragment(Fragments.LogIn);
                this._fragment = new LogInFragment();
                break;
            case 4:
                CurrentFragment(Fragments.AllGames);
                this._fragment = new AllGamesFragment();
                break;
            case 3:
                CurrentFragment(Fragments.Reviews);
                this._fragment = new ReviewsFragment();
                break;
            case 2:
                CurrentFragment(Fragments.GameProfile);
                this._fragment = new GameProfileFragment();
                break;
            case 1:
            default:
                CurrentFragment(Fragments.Profile);
                this._fragment = new ProfileFragment();
                break;
        }
        ContainerID(containerID);
        this._fragmentManager.beginTransaction().replace(_containerID, this._fragment).commit();
    }

    public void setDefaultFragment(int containerID){
        CurrentFragment(Fragments.Profile);
        this._fragment = new LogInFragment();
        ContainerID(containerID);
        this._fragmentManager.beginTransaction().replace(_containerID, this._fragment).commit();
    }

    public void setDefaultFragment(){
        if (!_rememberedContainer)
            return;

        this._fragment = new LogInFragment();
        CurrentFragment(Fragments.LogIn);
        this._fragmentManager.beginTransaction().replace(_containerID, this._fragment).commit();
    }

    public ApplicationFragmentManager saveContainer(int containerID){
        ContainerID(containerID);
        _rememberedContainer = true;
        return this;
    }

    public static Fragments CurrentFragment() {
        return _currentFragment;
    }

    private static void CurrentFragment(Fragments currentFragment) {
        _currentFragment = currentFragment;
    }

    public static int ContainerID(){
        return _containerID;
    }

    private static void ContainerID(int containerID){
        _containerID = containerID;
    }
}
