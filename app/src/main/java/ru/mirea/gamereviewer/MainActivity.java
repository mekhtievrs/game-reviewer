package ru.mirea.gamereviewer;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import ru.mirea.gamereviewer.Helpers.Managers.ApplicationFragmentManager;
import ru.mirea.gamereviewer.WebService.API.ApiTask;

public class MainActivity extends AppCompatActivity{
    private DrawerLayout drawerLayout;
    private NavigationView sideNavigationView;
    private NavController navController;
    public MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instance = this;

        initializeNavigation();

        // Инициализация бокового меню
        initializeSideNavigationPanel();

        // Инициализация нижнего меню
        initializeBottomNavigationPanel();

        // Фоновый поток для работы с API ПЕТЯ НЕ ТРОГАЙ ПОКА ЭТО!!!!!!!!!!!!!!!!!!!!
        ApiTask apiTask = new ApiTask();
        apiTask.execute();

    }

    private void initializeFirstPage()
    {
        ApplicationFragmentManager applicationFragmentManager = new ApplicationFragmentManager(getSupportFragmentManager());
        applicationFragmentManager.saveContainer(R.id.main_container).setDefaultFragment();
    }

    private void initializeNavigation()
    {
        navController = Navigation.findNavController(this, R.id.main_container);
    }

    private void initializeSideNavigationPanel()
    {
        // Инициализация Тулбара
        Toolbar toolbar = findViewById(R.id.reviews_toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);

        NavigationUI.setupWithNavController(toolbar, navController, drawerLayout);

        // Инициализируем наше боковое меню и назначем метод, который будет реагировать на нажатие кнопок
        sideNavigationView = findViewById(R.id.side_navigation_view);
        sideNavigationView.setNavigationItemSelectedListener(item -> {
            selectDrawerItem(item);
            return true;
        });

        sideNavigationView.getMenu().setGroupEnabled(R.id.group_side_menu, false);

        // Инициализируем поведение шапки нашего бокового меню
        initializeSideNavigationHeaderHandler();
    }

    private void initializeBottomNavigationPanel()
    {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        bottomNavigationView.setBackground(null);
        bottomNavigationView.getMenu().getItem(2).setEnabled(false);
        bottomNavigationView.getMenu().setGroupEnabled(R.id.group_bottom_menu, false);

        NavigationUI.setupWithNavController(bottomNavigationView, navController);
    }

    private void initializeSideNavigationHeaderHandler()
    {
        // Если у меню есть шапка, то инициализируем её, иначе выдаём ошибку
        View navigationHeaderView;

        if(sideNavigationView.getHeaderCount() > 0)
            navigationHeaderView = sideNavigationView.getHeaderView(0);
        else
            throw new NullPointerException("Navigation View Header is nullable!");

        navigationHeaderView.setEnabled(false);

        // При нажатии на шапку меню загружаем фрагмент с информацией о пользователе
        navigationHeaderView.setOnClickListener(v -> {
            navController.navigate(R.id.navigation_profile);
            drawerLayout.closeDrawer(GravityCompat.START);
        });
    }

    // Переключение фрагментов в зависимости от нажатия кнопки в меню
    @SuppressLint("NonConstantResourceId")
    private void selectDrawerItem(@NonNull MenuItem item) {
        switch(item.getItemId()){
            case R.id.nav_catalog:
                navController.navigate(R.id.navigation_profile);
                break;
            case R.id.nav_games:
                navController.navigate(R.id.navigation_all_games);
                break;
            case R.id.nav_reviews:
                navController.navigate(R.id.navigation_reviews);
                break;
            default:
                break;
        }

        item.setChecked(true);
        drawerLayout.closeDrawer(GravityCompat.START);
    }

    // Поведение при нажатии кнопки "Назад"
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }
    }


    public void openGameProfile(){
        navController.navigate(R.id.action_navigation_all_games_to_navigation_game_profile);
    }

    public void openReviewProfile(){
        navController.navigate(R.id.action_navigation_reviews_to_navigation_review_profile);
    }



}